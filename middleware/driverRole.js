const User = require('../models/userModel');

module.exports = async (req, res, next) => {
  const user = await User.findById(req.userId);

  if (user.role !== 'DRIVER') {
    return res.status(400).json({
      message: 'Only for driver available',
    });
  }
  next();
};
