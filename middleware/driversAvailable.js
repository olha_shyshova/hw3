const Truck = require('../models/truckModel');

module.exports = async (req, res, next) => {
  const trucks = await Truck.find({status: 'IS', assigned_to: {$nin: [null]}});
  if (trucks.length === 0) {
    return res.status(400).json({message: 'No driver available'});
  }
  req.trucks = trucks;
  next();
};
