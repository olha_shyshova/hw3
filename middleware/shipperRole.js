const User = require('../models/userModel');

module.exports = async (req, res, next) => {
  const user = await User.findById(req.userId);

  if (user.role !== 'SHIPPER') {
    return res.status(400).json({
      message: 'Only for a shipper available',
    });
  }
  next();
};
