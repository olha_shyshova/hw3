const express = require('express');
require('dotenv').config();
const mongoose = require('mongoose');
const app = express();
const PORT = +process.env.PORT || 8080;
const database = process.env.DB;

// import Routes
const authRoute = require('./routes/authRoute');
const usersRoute = require('./routes/userRoute');
const trucksRoute = require('./routes/truckRoute');
const loadsRoute = require('./routes/loadRoute');

// middleware
app.use(express.json());

// Route middleware
app.use('/api/auth', authRoute);
app.use('/api/users', usersRoute);
app.use('/api/trucks', trucksRoute);
app.use('/api/loads', loadsRoute);

const start = async () => {
  try {
    // connect to db
    await mongoose.connect(database, {useNewUrlParser: true});

    app.listen(PORT, () => console.log(`server runs http://localhost:${PORT}`));
  } catch (e) {
    console.log(e);
  }
};

start();
