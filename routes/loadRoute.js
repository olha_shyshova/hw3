const {verifyToken} = require('../controllers/authController');
const controller = require('../controllers/loadController');
const checkDriverRole = require('../middleware/driverRole');
const driversAvailable = require('../middleware/driversAvailable');
const shipperRole = require('../middleware/shipperRole');
const {validateLoad} = require('../middleware/validation');
const {Router} = require('express');
const router = new Router();

router.post('/', verifyToken, shipperRole, validateLoad,
    controller.addLoad,
);
router.get('/', verifyToken, controller.getLoads);
router.get('/active', verifyToken, checkDriverRole, controller.getActiveLoad);
router.patch('/active/state', verifyToken,
    checkDriverRole,
    controller.iterateLoadState,
);
router.post('/:id/post', verifyToken, driversAvailable, controller.postLoad);
router.get('/:id', verifyToken, shipperRole, controller.getLoad);
router.put('/:id', verifyToken, shipperRole, controller.updateLoad);
router.delete('/:id', verifyToken, shipperRole, controller.deleteLoad);
router.get('/:id/shipping_info', verifyToken,
    shipperRole,
    controller.getLoadInfo,
);

module.exports = router;
