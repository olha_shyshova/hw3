const {Router} = require('express');
const router = new Router();
const authController = require('../controllers/authController');
const {validateUser} = require('../middleware/validation');

router.post('/login', authController.login);
router.post('/register', validateUser, authController.register);
router.post('/forgot_password', authController.forgotPassword);

module.exports = router;
