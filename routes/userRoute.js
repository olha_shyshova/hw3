const {Router} = require('express');
const router = new Router();
const {verifyToken} = require('../controllers/authController');
const userController = require('../controllers/userController');

router.get('/me', verifyToken, userController.getUserInfo);
router.delete('/me', verifyToken, userController.deleteUser);
router.patch('/me/password', verifyToken, userController.changePassword);

module.exports = router;
