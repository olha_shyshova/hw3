const {Schema, model} = require('mongoose');
const {loadStatus, loadStates} = require('../utils/config');

const schema = new Schema({
  name: {type: String, required: true},
  created_by: {type: String, required: true},
  assigned_to: {type: String, default: null},
  payload: {type: Number, required: true},
  pickup_address: {type: String, required: true},
  delivery_address: {type: String, required: true},
  dimensions: {
    width: Number,
    length: Number,
    height: Number,
  },
  status: {type: String, enum: loadStatus, default: 'NEW'},
  state: {type: String, enum: loadStates},
  logs: [
    {
      message: String,
      time: {type: String, default: new Date().toISOString()},
    },
  ],
  created_date: {type: String, default: new Date().toISOString()},
});

module.exports = model('Load', schema);
