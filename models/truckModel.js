const {Schema, model} = require('mongoose');
const {truckTypes, truckStatus} = require('../utils/config');

const schema = new Schema({
  created_by: {type: String, required: true},
  assigned_to: {type: String, default: null},
  type: {type: String, enum: truckTypes, required: true},
  status: {type: String, enum: truckStatus, required: true},
  created_date: {type: String, default: new Date().toISOString()},
});

module.exports = model('Truck', schema);
