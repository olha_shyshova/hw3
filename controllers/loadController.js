const Load = require('../models/loadModel');
const {truckDimensions} = require('../utils/config');
const {loadStates} = require('../utils/config');
const Truck = require('../models/truckModel');
const User = require('../models/userModel');

class LoadController {
  async addLoad(req, res) {
    try {
      await Load.create({
        ...req.body,
        created_by: req.userId,
        status: 'NEW',
        logs: [{message: 'Load created, status NEW'}],
      });
      res.status(200).json({message: 'Load created successfully'});
    } catch (e) {
      res.status(500).json({message: e.message});
    }
  }

  async getLoads(req, res) {
    let {status, limit, offset} = req.query;
    limit = limit || 10;
    offset = offset || 0;
    try {
      const user = await User.findById(req.userId);
      let loads = null;

      if (user.role === 'SHIPPER') {
        loads = await Load.find({created_by: req.userId});
      } else {
        loads = await Load.find({assigned_to: req.userId});
      }

      if (status) {
        loads = loads.filter((load) => load.status === status);
      }
      loads = loads.splice(offset, limit);
      res.status(200).json({loads});
    } catch (e) {
      res.status(500).json({message: e.message});
    }
  }

  async postLoad(req, res) {
    const load = await Load.findById(req.params.id);
    try {
      load.status = 'POSTED';
      load.logs = [...load.logs, {message: 'Load status POSTED'}];
      await load.save();

      for (const truck of req.trucks) {
        if (truckDimensions(load, truck)) {
          truck.status = 'OL';
          load.status = 'ASSIGNED';
          load.state = 'En route to Pick Up';
          load.assigned_to = truck.assigned_to;
          load.logs = [...load.logs, {message: 'Load status ASSIGNED'}];
          await load.save();
          await truck.save();
          return res.status(200).json({
            message: 'Load posted successfully',
            driver_found: true,
          });
        } else {
          res.status(400).json({message: 'Dimensions are not valid'});
          load.status = 'NEW';
          load.logs = [...load.logs, {message: 'Load status NEW'}];
          await load.save();
        }
      }
    } catch (e) {
      res.status(500).json({message: e.message});
    }
  }

  async getActiveLoad(req, res) {
    try {
      const load = await Load.findOne({
        assigned_to: req.userId,
        status: {$nin: ['SHIPPED']},
      });
      res.status(200).json({load});
    } catch (e) {
      res.status(500).json({message: e.message});
    }
  }

  async iterateLoadState(req, res) {
    try {
      const load = await Load.findOne({
        assigned_to: req.userId,
        status: {$nin: ['SHIPPED']},
      });
      let loadState = load.state;

      if (loadState === 'Arrived to delivery') {
        return res
            .status(400).json({message: 'Load already arrived'});
      }

      const k = loadStates.indexOf(loadState);
      loadState = loadStates[k + 1];
      load.state = loadState;

      if (loadState === 'Arrived to delivery') {
        load.status = 'SHIPPED';
        load.logs = [...load.logs, {message: 'Load successfully SHIPPED'}];
        await Truck.updateOne({assigned_to: req.userId}, {status: 'IS'});
      }

      await load.save();
      res.status(200).json({message: `Load state changed to ${loadState}`});
    } catch (e) {
      console.log(e);
      res.status(500).json({message: e.message});
    }
  }

  async getLoad(req, res) {
    try {
      const load = await Load.findById(req.params.id);
      if (!load) {
        res.status(400).json({message: 'Invalid load id'});
      }
      res.status(200).json({load});
    } catch (e) {
      res.status(500).json({message: e.message});
    }
  }

  async updateLoad(req, res) {
    try {
      await Load.updateOne({_id: req.params.id}, {...req.body});
      res.status(200).json({message: 'Load details changed successfully'});
    } catch (e) {
      console.log(e);
      res.status(500).json({message: e.message});
    }
  }

  async deleteLoad(req, res) {
    try {
      await Load.deleteOne({_id: req.params.id});
      res.status(200).json({message: 'Load deleted successfully'});
    } catch (e) {
      res.status(500).json({message: e.message});
    }
  }

  async getLoadInfo(req, res) {
    try {
      const load = await Load.findById(req.params.id);

      if (load.assigned_to === null) {
        res.status(200).json({load});
      }

      const truck = await Truck.findOne({assigned_to: load.assigned_to});
      res.status(200).json({load, truck});
    } catch (e) {
      res.status(500).json({message: e.message});
    }
  }
}

module.exports = new LoadController;
