const Truck = require('../models/truckModel');
const {truckTypes} = require('../utils/config');

class TruckController {
  async addTruck(req, res) {
    const {type} = req.body;
    if (!truckTypes.includes(type)) {
      res.status(400).json({message: 'Wrong truck type'});
    }

    try {
      await Truck.create({type, status: 'IS', created_by: req.userId});
      res.status(200).json({message: 'Truck created successfully'});
    } catch (e) {
      res.status(500).json({message: e.message});
    }
  }

  async getTrucks(req, res) {
    try {
      const trucks = await Truck.find({created_by: req.userId});
      res.status(200).json({trucks});
    } catch (e) {
      res.status(500).json({message: e.message});
    }
  }

  async getTruck(req, res) {
    try {
      const truck = await Truck.findById(req.params.id);
      if (!truck) {
        res.status(400).json({message: 'Truck does not exist'});
      }
      res.status(200).json({truck});
    } catch (e) {
      res.status(500).json({message: e.message});
    }
  }

  async updateTruck(req, res) {
    const {type} = req.body;
    const assignedTruck = await Truck.findOne({assigned_to: req.userId});
    if (assignedTruck.truckStatus === 'OL') {
      return res
          .status(400)
          .json({message: 'You can\'t do this while being on load'});
    }
    if (!truckTypes.includes(type)) {
      res.status(400).json({message: 'Wrong truck type'});
    }
    try {
      await Truck.updateOne({_id: req.params.id}, {type: req.body.type});
      res.status(200).json({message: 'Truck details changed successfully'});
    } catch (e) {
      res.status(500).json({message: e.message});
    }
  }

  async deleteTruck(req, res) {
    try {
      const assignedTruck = await Truck.findOne({assigned_to: req.userId});
      if (assignedTruck) {
        if (assignedTruck.status === 'OL') {
          return res
              .status(400)
              .json({message: 'You cannot do this while being on load'});
        }
      }
      await Truck.deleteOne({_id: req.params.id});
      res.status(200).json({message: 'Truck deleted successfully'});
    } catch (e) {
      res.status(500).json({message: e.message});
    }
  }

  async assignTruck(req, res) {
    try {
      const assignedTruck = await Truck.findOne({assigned_to: req.userId});
      if (assignedTruck) {
        if (assignedTruck.status === 'OL') {
          return res
              .status(400)
              .json({message: 'You cannot do this while being on load'});
        }
        assignedTruck.assigned_to = null;
        await assignedTruck.save();
      }

      await Truck.updateOne({_id: req.params.id}, {assigned_to: req.userId});
      res.status(200).json({message: 'Truck assigned successfully'});
    } catch (e) {
      res.status(500).json({message: e.message});
    }
  }
}

module.exports = new TruckController;
