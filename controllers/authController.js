require('dotenv').config();
const bcrypt = require('bcrypt');
const User = require('../models/userModel');
const jwt = require('jsonwebtoken');
const secret = process.env.TOKEN_SECRET;

class AuthController {
  async login(req, res) {
    const {email, password} = req.body;

    const user = await User.findOne({email});

    if (!user) {
      return res.status(400).json({message: 'User not found'});
    }

    const isPasswordValid = bcrypt.compareSync(password, user.password);

    if (!isPasswordValid) {
      return res.status(400).json({message: 'Invalid password!'});
    }

    const token = jwt.sign({id: user.id, email}, secret, {
      expiresIn: 60 * 60 * 12,
    });
    res.status(200).json({jwt_token: token});
  }

  async register(req, res) {
    const {email, password, role} = req.body;

    const userWithSameName = await User.findOne({email});
    if (userWithSameName) {
      return res
          .status(400)
          .json({message: 'User already exists'});
    }

    try {
      await User.create({
        role,
        email,
        password: bcrypt.hashSync(password, 7),
      });
      res.status(200).json({message: 'Profile created successfully'});
    } catch (e) {
      console.log(e);
      res.status(500).json({message: e.message});
    }
  }

  async verifyToken(req, res, next) {
    if (!req.headers.authorization) {
      return res.status(400).json({message: 'Invalid token'});
    }

    const token = req.headers.authorization.split(' ')[1] ?
    req.headers.authorization.split(' ')[1] :
    req.headers.authorization;

    try {
      const decoded = jwt.verify(token, secret);
      req.userId = decoded.id;
    } catch (e) {
      return res.status(400).json({message: 'Access denied'});
    }

    const user = await User.findOne({_id: req.userId});
    if (!user) {
      return res.status(400).json({message: 'Access denied'});
    }
    next();
  }

  async forgotPassword(req, res) {
    try {
      const {email} = req.body;
      const user = await User.findOne({email});
      if (!user) {
        return res.status(400)
            .send({message: 'Invalid email'});
      }
      res.status(200).send({
        message: 'New password sent to your email',
      });
    } catch (e) {
      res.status(500).send({message: e.message});
    }
  }
}

module.exports = new AuthController;
